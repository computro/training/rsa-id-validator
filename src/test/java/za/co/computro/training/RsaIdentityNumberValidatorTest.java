package za.co.computro.training;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class RsaIdentityNumberValidatorTest {

    private RsaIdentityNumberValidator validator = new RsaIdentityNumberValidator();

    @Test
    public void test_isValidRsaIdentityNumber_should_return_true_if_length_is_equal_to_13() {
        // given
        String id2 = "9902175466083";

        // When
        Boolean checkId2 = validator.validateLength(id2);

        // Then
        assertTrue(checkId2, "Id number should validate successfully");
    }

    @Test
    public void test_isValidRsaIdentityNumber_should_return_false_if_length_is_less_than_13() {
        // given
        String invalidIdNumber = "99021754663";

        // When
        Boolean checkId2 = validator.validateLength(invalidIdNumber);

        // Then
        assertFalse(checkId2, "Id number is less than 13 characters long");
    }
}