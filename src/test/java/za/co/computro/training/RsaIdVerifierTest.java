package za.co.computro.training;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class RsaIdVerifierTest {
    private RsaIdentityNumberValidator validator = new RsaIdentityNumberValidator();

    @BeforeEach
    public void setup() {
        System.out.println("Before the test run!");
    }

    @AfterEach
    public void cleanup() {
        System.out.println("After the test run!");
    }

    @Test
    public void test_verifyRsaIdentityNumber_should_verify_the_id_number_against_dha_api() {
        System.out.println("Running test");
        String validIdNumber = "9902175466083";
        DhaApiClient client = Mockito.mock(DhaApiClient.class);
        RsaIdVerifier verify = new RsaIdVerifier(client, validator);

        when(client.idNumberExist(validIdNumber, false)).thenThrow(RuntimeException.class);
        when(client.idNumberExist(validIdNumber, true)).thenReturn(true);

        boolean verified = verify.verifyRsaIdentityNumber(validIdNumber);

        verify(client, times(2)).idNumberExist(anyString(), anyBoolean());
        assertTrue(verified);
    }

    @Test
    public void test_verifyRsaIdentityNumber_should_return_false_if_an_invalid_idNumber_is_passed() {
        String validIdNumber = "99021754663";
        RsaIdentityNumberValidator rsaIdentityNumberValidator = Mockito.spy(validator);
        RsaIdVerifier verify = new RsaIdVerifier(null, rsaIdentityNumberValidator);

        // When
        boolean verified = verify.verifyRsaIdentityNumber(validIdNumber);

        // Then
        assertFalse(verified);
        verify(rsaIdentityNumberValidator).isValidRsaIdentityNumber(validIdNumber);
    }
}