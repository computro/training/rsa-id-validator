package za.co.computro.training;

public class RsaIdVerifier {
    private final DhaApiClient client; // Dependency
    private final RsaIdentityNumberValidator validator; // Dependency

    public RsaIdVerifier(DhaApiClient client, RsaIdentityNumberValidator validator) {
        this.client = client;
        this.validator = validator;
    }

    public boolean verifyRsaIdentityNumber(final String idNumber) {
        if (!validator.isValidRsaIdentityNumber(idNumber)) {
            return false;
        }
        try {
            return client.idNumberExist(idNumber, false);
        } catch (RuntimeException e) {
            return client.idNumberExist(idNumber, true);
        }
    }
}
