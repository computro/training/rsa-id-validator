package za.co.computro.training;

public class RsaIdentityNumberValidator {

    public final boolean isValidRsaIdentityNumber(String identityNumber) {
        return validateLength(identityNumber) && validateLuhn(identityNumber);
    }

    protected boolean validateLength(String identityNumber) {
        if (identityNumber.length() != 13) {
            return false;
        }
        return true;
    }

    protected boolean validateLuhn(String identityNumber) {
        int checkDigit = Integer.parseInt(identityNumber.substring(12));
        int luhnSum = getLuhnSum(identityNumber);
        luhnSum *= 9;
        String value = String.valueOf(luhnSum);
        int confirmDigit = Integer.parseInt(value.substring(value.length() - 1));
        if (confirmDigit != checkDigit) {
            return false;
        }
        return true;
    }

    protected int getLuhnSum(String idNumber) {
        char[] digits = idNumber.toCharArray();
        int sum = 0;
        for (int i = 12; i > 0; i--) {
            int digit = Character.getNumericValue(digits[i - 1]);
            if (i % 2 == 0) {
                digit = doubleDigitLuhn(digit);
            }
            sum += digit;
        }
        return sum;
    }

    protected int doubleDigitLuhn(int digit) {
        digit *= 2;
        if (digit > 9) {
            digit = digit - 9;
        }
        return digit;
    }
}
